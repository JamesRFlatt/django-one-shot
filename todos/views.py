from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList, TodoItem
from .forms import PostForm, ItemPostForm

# Create your views here.


def view_todos(request):
    context = {"todos": TodoList.objects.all}
    return render(request, "todos/lists.html/", context)


def view_todo_details(request, id):
    specific_todo = get_object_or_404(TodoList, id=id)
    context = {"todo_list_object": specific_todo}
    return render(request, "todos/detail.html/", context)


def todo_list_create(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            task = form.save()
            return redirect("todo_list_detail", id=task.id)
    else:
        form = PostForm()

    context = {"post_form": form}

    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = PostForm(request.POST, instance=post)
        if form.is_valid():
            task = form.save()
            return redirect("todo_list_detail", id=task.id)
    else:
        form = PostForm(instance=post)

    context = {"post_form": form}
    return render(request, "todos/update.html", context)


def todo_list_delete(request, id):
    to_delete = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        to_delete.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = ItemPostForm(request.POST)
        if form.is_valid():
            task = form.save()
            return redirect("todo_list_detail", id=task.list.id)
    else:
        form = ItemPostForm()

    context = {"item_post_form": form}
    return render(request, "todos/create_item.html", context)


def todo_item_update(request, id):
    post = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = ItemPostForm(request.POST, instance=post)
        if form.is_valid():
            task = form.save()
            return redirect("todo_list_detail", id=task.list.id)
    else:
        form = ItemPostForm()

    context = {"item_post_form": form}
    return render(request, "todos/update_item.html", context)
